import random
import sympy

def trivialSharingEncryption(s, n, k):
   fragments = []
   for x in range(0, n - 1):  # losuje fragmenty
       fragments.append(random.randrange(0, k))
   last = int(s)  # ostatni równa się sekretowi
   for x in fragments:  # oblicza ostatni fragment
       last = last - x
   last = last % k
   fragments.append(last)  # dodaje ostatni fragemnt do listy
   print('Fragmenty: ' + str(fragments))
   return fragments

def trivalSharingDecryption(fragments, k):
   s = 0
   for fragment in fragments:
       s = s + fragment
   s = s % k
   return s

def polynomialCounter(k, ListA, x):
   a0 = ListA[0]
   polynomial = a0 #przypisanie sekretu
   if k == 0:
       print('Nie ma takiego stopnia wielomianu')
   else:
       i = 1
       for j in range(k-1):
           polynomial = polynomial + ListA[i]*pow(x, i)
           i = i+1
   return polynomial

def ShamirEncryption(s, n, k):
  listA = [s]
  bool = 0
  p = 0
  listOfPolynomials = []
  if s > n:
      max = s
  else:
      max = n
  print('Wylosować liczbę pierwszą? Tak - 1, Nie - 2')
  ans = input('Podaj odpowiedź: ')
  if ans == str(1):
       p = sympy.randprime(max, int(max)+1000000) #losuje wartość p
  elif ans == str(2):
      while bool == 0:
           kk = sympy.randprime(max, int(max) + 1000000)  # losuje wartość p
           print('Podpowiedź: ' + kk)
           p = int(input('Podaj liczbę pierwszą: '))
           if p > s and p > n and sympy.isprime(p):
               print('Liczba spełnia warunki')
               bool = 1
           else:
               print('Liczba nie spełnia warunków')

  print()
  print('Wartość p :' + str(p))
  print()
  for i in range(0,int(k)-1):
      a = random.randrange(0,p)
      print('Wylosowana wartość a' + str(i) + ': ' + str(a))
      listA.append(a)                        #dodaje wylosowane wartosci a
  i = 1
  print()
  print('Punkty: ')
  for j in range(n):
      polynomial = polynomialCounter(k, listA, i)
      print('D' + str(i-1) + ': ' + '(' + str(i) + ',' + str(polynomial) + ')')
      i = i + 1
      listOfPolynomials.append(polynomial)
  print()
  print('Współczynniki wielomianu: ' + str(listA))
  print()
  return listOfPolynomials


class Point:
   def __init__(self, x, y):
       self.x = x
       self.y = y

def interpolate(f: list, xi: int, n: int) -> float:
   result = 0.0
   for i in range(n):
       var = f[i].y
       for j in range(n):
           if j != i:
               var = var * (xi - f[j].x) / (f[i].x - f[j].x)
       result += var
   return result

def toData(listOfPolynomials):
   ListOfPoints = []
   for i in range(len(listOfPolynomials)):
       y = i+1
       z = listOfPolynomials[i]
       ListOfPoints.append(Point(y, z))
   return ListOfPoints

while(True):
   print(' ')
   print('MENU')
   print('Wybierz numer zadania: ')
   print('1 - Trywialna metoda')
   print('2 - Shamir\n')
   nr = int(input('Wprowadz numer: '))
   if nr < 1 or nr > 2:
       print('Zly numer zadania, podaj jeszcze raz')
       continue
   else:
       if nr == 1:
           print(' ')
           print('TRYWIALNA METODA DZIELENIA SEKRETU - SZYFROWANIE')
           k = int(input('Podaj maks przedziału: '))
           s = input('Podaj sekret z przedziału 0-' + str(k) + ': ')
           n = int(input('Podaj ile ma być fragmentów: '))
           fragments = trivialSharingEncryption(s, n, k)
           print(' ')
           print('TRYWIALNA METODA DZIELENIA SEKRETU - DESZYFROWANIE')
           secret = trivalSharingDecryption(fragments, k)
           print('Sekret po odszyfrowaniu: ' + str(secret))
           if str(s) == str(secret):
               print('Sekret przed zaszyfrowaniem i po odszyfrowaniu: RÓWNE')
           else:
               print('Sekret przed zaszyfrowaniem i po odszyfrowaniu: RÓŻNE')
           input()
       elif nr == 2:
           print(' ')
           print('SCHEMAT SHAMIRA - SZYFROWANIE')
           s = input('Podaj sekret: ')
           n = int(input('Podaj ile ma być fragmentów: '))
           k = int(input('Podaj stopień wielomianu: '))
           listOfPolynomials = ShamirEncryption(int(s), int(n), int(k))
           listOfPoints = toData(listOfPolynomials)
           print(' ')
           print('SCHEMAT SHAMIRA - DESZYFROWANIE')
           k = int(input('Podaj stopień wielomianu: '))
           secret = int(interpolate(listOfPoints, 0, k))
           print('Sekret po odszyfrowaniu: ' + str(secret))
           if str(s) == str(secret):
               print('Sekret przed zaszyfrowaniem i po odszyfrowaniu: RÓWNE')
           else:
               print('Sekret przed zaszyfrowaniem i po odszyfrowaniu: RÓŻNE')
           input()


